from django.test import TestCase
from booksharing_api.api.models import Genre


class GenreModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Genre.objects.create(
            name='Роман',
            short_description='Большое повествовательное художественное произведение со сложным сюжетом.'
        )

    def test_name(self):
        genre = Genre.objects.get(id=1)
        name = genre.name
        self.assertEquals(name, 'Роман')

    def test_name_max_length(self):
        genre = Genre.objects.get(id=1)
        max_length = genre._meta.get_field('name').max_length
        self.assertEquals(max_length, 150)

    def test_short_description(self):
        genre = Genre.objects.get(id=1)
        short_description = genre.short_description
        self.assertEquals(
            short_description, 'Большое повествовательное художественное произведение со сложным сюжетом.'
        )

    def test_short_description_max_length(self):
        genre = Genre.objects.get(id=1)
        max_length = genre._meta.get_field('short_description').max_length
        self.assertEquals(max_length, 60)

    def test_short_desription_default_value(self):
        genre = Genre.objects.get(id=1)
        default_value = genre._meta.get_field('short_description').default
        self.assertEquals(default_value, '-')
