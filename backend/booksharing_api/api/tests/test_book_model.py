from django.test import TestCase
from django.contrib.auth.models import User
from booksharing_api.api.models import Book


class BookModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        book = Book.objects.create(
            name='Метро 2033',
            description='Книга о постапокалипсисе',
            owner=User.objects.create_user(username='NRE186')
        )
        book.save()
        genre1 = book.genres.create(
            name='Роман',
            short_description='Большое повествовательное художественное произведение со сложным сюжетом.'
        )
        genre1.save()
        genre2 = book.genres.create(
            name='Антиутопия',
            short_description='-'
        )
        genre2.save()

    def test_name(self):
        book = Book.objects.get(id=1)
        name = book.name
        self.assertEquals(name, 'Метро 2033')

    def test_name_max_length(self):
        book = Book.objects.get(id=1)
        length = book._meta.get_field('name').max_length
        self.assertEquals(length, 200)

    def test_description(self):
        book = Book.objects.get(id=1)
        description = book.description
        self.assertEquals(description, 'Книга о постапокалипсисе')

    def test_description_max_length(self):
        book = Book.objects.get(id=1)
        length = book._meta.get_field('description').max_length
        self.assertEquals(length, 1000)

    def test_owner_username(self):
        book = Book.objects.get(id=1)
        owner = book.owner.username
        self.assertEquals(owner, "NRE186")

# TODO: написать тесты для авторов и жанров
