from django.test import TestCase
from booksharing_api.api.models import Author


class AuthorModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Author.objects.create(
            name='Дмитрий',
            surname='Глуховский'
        )

    def test_name(self):
        author = Author.objects.get(id=1)
        name = author.name
        self.assertEquals(name, 'Дмитрий')

    def test_name_max_length(self):
        author = Author.objects.get(id=1)
        max_length = author._meta.get_field('name').max_length
        self.assertEquals(max_length, 50)

    def test_surname(self):
        author = Author.objects.get(id=1)
        surname = author.surname
        self.assertEquals(surname, 'Глуховский')

    def test_surname_max_length(self):
        author = Author.objects.get(id=1)
        max_length = author._meta.get_field('surname').max_length
        self.assertEquals(max_length, 100)
