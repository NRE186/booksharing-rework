from django.contrib import admin
from .models import Genre, Author, Book


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    pass


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('name', 'short_description')


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'display_authors', 'display_genres', 'owner')
