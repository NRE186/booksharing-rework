import graphene

from django.contrib.auth.models import User
from graphene_django.types import DjangoObjectType, ObjectType
from .models import Author, Genre, Book

"""Типы данных для GraphQl"""


class AuthorType(DjangoObjectType):
    class Meta:
        model = Author


class GenreType(DjangoObjectType):
    class Meta:
        model = Genre


class BookType(DjangoObjectType):
    class Meta:
        model = Book


class Query(ObjectType):
    author = graphene.Field(AuthorType, id=graphene.Int())
    genre = graphene.Field(GenreType, id=graphene.Int())
    book = graphene.Field(BookType, id=graphene.Int())
    authors = graphene.List(AuthorType)
    genres = graphene.List(GenreType)
    books = graphene.List(BookType)

    def resolve_author(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Author.objects.get(pk=id)

        return None

    def resolve_genre(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Genre.objects.get(pk=id)

        return None

    def resolve_book(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Book.objects.get(pk=id)

        return None

    def resolve_authors(self, info, **kwargs):
        return Author.objects.all()

    def resolve_genres(self, info, **kwargs):
        return Genre.objects.all()

    def resolve_books(self, info, **kwargs):
        return Book.objects.all()


"""Мутации"""


class AuthorInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    surname = graphene.String()


class GenreInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    short_description = graphene.String()


class BookInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    description = graphene.String()
    authors = graphene.List(AuthorInput)
    genres = graphene.List(GenreInput)
    owner = graphene.ID()


class CreateAuthor(graphene.Mutation):
    class Arguments:
        input = AuthorInput(required=True)

    ok = graphene.Boolean()
    author = graphene.Field(AuthorType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        author_instance = Author(name=input.name, surname=input.surname)
        author_instance.save()
        return CreateAuthor(ok=ok, author=author_instance)


class UpdateAuthor(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = AuthorInput(required=True)

    ok = graphene.Boolean()
    author = graphene.Field(AuthorType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        author_instance = Author.objects.get(pk=id)
        if author_instance:
            ok = True
            author_instance.name = input.name
            author_instance.surname = input.surname
            author_instance.save()
            return UpdateAuthor(ok=ok, actor=author_instance)
        return UpdateAuthor(ok=ok, actor=None)


class CreateGenre(graphene.Mutation):
    class Arguments:
        input = GenreInput(required=True)

    ok = graphene.Boolean()
    genre = graphene.Field(GenreType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        genre_instance = Author(name=input.name, surname=input.short_description)
        genre_instance.save()
        return CreateGenre(ok=ok, author=genre_instance)


class UpdateGenre(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = GenreInput(required=True)

    ok = graphene.Boolean()
    genre = graphene.Field(GenreType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        genre_instance = Genre.objects.get(pk=id)
        if genre_instance:
            ok = True
            genre_instance.name = input.name
            genre_instance.surname = input.surname
            genre_instance.save()
            return UpdateGenre(ok=ok, actor=genre_instance)
        return UpdateGenre(ok=ok, actor=None)


class CreateBook(graphene.Mutation):
    class Arguments:
        input = BookInput(required=True)

    ok = graphene.Boolean()
    book = graphene.Field(BookType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        authors = []
        genres = []
        for author_input in input.authors:
            author = Author.objects.get(pk=author_input.id)
            authors.append(author)
        for genre_input in input.genres:
            genre = Genre.objects.get(pk=genre_input.id)
            genres.append(genre)
        owner = User.objects.get(pk=input.owner)
        book_instance = Book(
            name=input.name,
            description=input.description,
            owner=owner
        )
        book_instance.save()
        book_instance.authors.set(authors)
        book_instance.genres.set(genres)
        return CreateBook(ok=ok, book=book_instance)


class UpdateBook(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = BookInput(required=True)

    ok = graphene.Boolean()
    book = graphene.Field(BookType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        movie_instance = Book.objects.get(pk=id)
        if movie_instance:
            ok = True
            authors = []
            genres = []
            for author_input in input.authors:
                author = Genre.objects.get(pk=author_input.id)
                authors.append(author)
            for genre_input in input.genres:
                genre = Genre.objects.get(pk=genre_input.id)
                genres.append(genre)
            movie_instance = Book(
                title=input.title,
                year=input.year
            )
            book_instance = Book(
                name=input.name,
                description=input.description,
                owner=input.owner
            )
            book_instance.save()
            book_instance.authors.set(authors)
            book_instance.genres.set(genres)
            return UpdateBook(ok=ok, movie=movie_instance)
        return UpdateBook(ok=ok, movie=None)


class Mutation(graphene.ObjectType):
    create_author = CreateAuthor.Field()
    update_author = UpdateAuthor.Field()
    create_genre = CreateGenre.Field()
    update_genre = UpdateGenre.Field()
    create_book = CreateBook.Field()
    update_book = UpdateBook.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
