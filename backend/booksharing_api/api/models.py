from django.contrib.auth.models import User
from django.db import models


class Author(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=50)
    surname = models.CharField(verbose_name='Фамилия', max_length=100)

    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return f"{self.name} {self.surname}"


class Genre(models.Model):
    name = models.CharField(verbose_name='Название жанра', max_length=150)
    short_description = models.CharField(verbose_name='Короткое описание жанра', max_length=60, default='-')

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'

    def __str__(self):
        return self.name


class Book(models.Model):
    name = models.CharField(verbose_name='Название книги', max_length=200)
    description = models.CharField(verbose_name='Описание книги', max_length=1000)
    authors = models.ManyToManyField(Author, verbose_name='Авторы книги')
    genres = models.ManyToManyField(Genre, verbose_name='Жанры')
    owner = models.ForeignKey(User, verbose_name='Владелец', on_delete=models.CASCADE, default='0')

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'

    def __str__(self):
        return self.name

    def display_authors(self):
        return ', '.join([f'{authors.name} {authors.surname}' for authors in self.authors.all()[:3]])
    display_authors.short_description = 'Авторы'

    def display_genres(self):
        return ', '.join([genre.name for genre in self.genres.all()[:3]])
    display_genres.short_description = 'Жанры'
